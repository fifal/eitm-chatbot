Aplikace prozatím běží na [chatbot.filek.cz](https://chatbot.filek.cz), kde je možné ji vyzkoušet.

![Chatbot](app.png)

# Zprovoznění aplikace

## 1. Příprava pro rozpoznávání jazyka (LUIS)
V rootu projektu:
```powershell
az login # přihlášení se do azure console
az group create --name eitm-chatbot --location "westus" # vytvoření resource group
az deployment group create --name luis-deployment --resource-group eitm-chatbot --template-file deployment-templates/luis-deploy.json # deploy ze šablony
```

## 2. Nahrání natrénovaného modelu do LUIS
**Je potřeba počkat cca 5 minut od prvního kroku, než se vše na Azure připraví**
- Pak jít na [preview.luis.ai](https://preview.luis.ai) 
- New App -> Import JSON -> vybrat soubor `deployment-templates/models/luis_model.json`
- V nově vytvořené aplikaci kliknout na tlačítko "Train" a dále "Publish" na Production
- V záložce "Manage" -> "Azure Resources": <br>-> zkopírovat Primary key a vložit ho do `python-bot/config.example.py` k hodnotě `LUIS_API_KEY`
    <br>-> zkopírovat Endpoint URL a vložit do téhož souboru do `LUIS_API_HOST_NAME`
- V záložce "Application Settings" zkopírovat AppID a vložit do téhož souboru do `LUIS_APP_ID`
- Nastavit `IMAGE_SERVER` na url, kde poběží nette-aplikace
- Soubor uložit jako `python-bot/config.py`

## 3. Nepovinné (QnA Bot pro často kladené otázky)
```powershell
az group create --name eitm-qnabot --location "westus"
az deployment group create --name qna-deployment --resource-group eitm-qnabot --template-file deployment-templates/qna-deploy.json --parameters name="eitm-qnabot" appName="eitm-qnabot" location="westus" location3="westus" location4="westus" sku="F0" sku2="free" searchHostingMode="Default"
```
- Na adrese [qnamaker.ai](https://qnamaker.ai) -> Create a knowledge base:
    - Step 1 -> přeskočit (bylo vytvořeno příkazem výše)
    - Step 2 -> vyplnit vše a QnA Service nastavit na eitm-qnabot
    - Step 3 -> libovolné jméno
    - Step 4 -> Add file -> `deployment-templates/models/qna_knowledge_base.tsv`
    - Step 5 -> Create -> Sava and Train -> Publish
        - z Tabulky výpisu ukázkového příkazu zkopírovat:
            - endpoint: `/knowledgebases/..../generateAnswer` a vložit do `nette-app/app/config/local.example.neon` do `knowledge_base`
            - Host: `https://eitm-qnabot.azurewebsites.net/qnamaker` a vložit do téhož soboru do `endpoint`
            - EndpointKey: zkopírovat ID klíče do uložit do `auth_key`
            - v local.example.neon nastavit enabled na true a uložit soubor jako local.neon

## 4. Deploy Python bota:
- Přejít do složky `python-bot/`
- Zabalit vše ve složce do `python-bot.zip`
```powershell
az ad app create --display-name "eitm-python-bot" --password "AtLeastSixteenCharacters_0" --available-to-other-tenants
```
- Tento krok vygeneruje App ID, které je třeba si zkopírovat, bude potřeba dále:
```powershell
az deployment create --template-file ".\deployment_templates\template-with-new-rg.json" --location westeurope --parameters appId="<ID Z PREDCHOZIHO KROKU>" appSecret="AtLeastSixteenCharacters_0" botId="python-bot-eitm" botSku=F0 newAppServicePlanName="python-bot-eitm" newWebAppName="python-bot-eitm" groupName="python-bot-eitm" groupLocation="westeurope" newAppServicePlanLocation="westeurope" --name "python-bot-eitm"
```
- A nakonec deploy:
```powershell
az webapp deployment source config-zip --resource-group "python-bot-eitm" --name "python-bot-eitm" --src "python-bot.zip"
```

## 5. Získání channel klíčů chatbota
- Jít na [portal.azure.com](https://portal.azure.com)
- Najít v Resources Groups skupinu `python-bot-eitm`
- Ve skupině najít `python-bot-eitm Bot Channels Registration`
    - přejít do "Channels" -> na "Web Chat" kliknout na edit
    - zkopírovat první secret key a vložit ho do `nette-app/app/config/local.neon` do `python_chatbot.secret_key`
    - zkopírovat z "Embed code" endpoint ze `src` bez parametrů (https://webchat.botframework.com/embed/python-bot-eitm) a vložit do `local.neon` do `python_chatbot.endpoint`

## 6. Spuštění nette-app
- V rootovske složce projektu:
```powershell
docker-compose up -d --build
docker exec -it nette-app bash
```
- Dostaneme se do kontajneru:
```bash
composer install
cd www
bower install --allow-root
```

Aplikace pak běží na localhostu:80, na úvodní stránce je Chatbot pro výběr matrace, ve FAQ je pak QnA Bot