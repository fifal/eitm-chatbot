# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT License.
import json

from botbuilder.core import ActivityHandler, TurnContext, ConversationState, UserState, MessageFactory, CardFactory
from botbuilder.schema import Activity, ActivityTypes
from data_models import UserProfile, Question, ConversationFlow, MattressData, MattressFlow, MattressQuestion
from mattress_recognizer import MattressRecognizer
from helpers import LuisHelper, Intent, MattressChooser
from config import DefaultConfig


class EitmChatbot(ActivityHandler):
    def __init__(self,
                 conversation_state: ConversationState,
                 user_state: UserState,
                 luis_recognizer: MattressRecognizer,
                 config: DefaultConfig):
        self.config = config
        self.conversation_state = conversation_state
        self.user_state = user_state
        self.luis_recognizer = luis_recognizer

        self.flow_accessor = self.conversation_state.create_property("ConversationFlow")
        self.profile_accessor = self.conversation_state.create_property("UserProfile")

        self.mattress_flow_accessor = self.conversation_state.create_property("MattressFlow")
        self.mattress_accessor = self.conversation_state.create_property("MattressData")

    async def on_message_activity(self, turn_context: TurnContext):
        profile = await self.profile_accessor.get(turn_context, UserProfile)
        flow = await self.flow_accessor.get(turn_context, ConversationFlow)

        mattress_data = await self.mattress_accessor.get(turn_context, MattressData)
        mattress_flow = await self.mattress_flow_accessor.get(turn_context, MattressFlow)

        if mattress_flow.last_question_asked != MattressQuestion.DONE:
            await self._get_mattress_data(mattress_flow, mattress_data, turn_context)
        else:
            await self._fill_out_user_profile(flow, profile, turn_context)

        # Save changes to UserState and ConversationState
        await self.conversation_state.save_changes(turn_context)
        await self.user_state.save_changes(turn_context)

    async def _get_mattress_data(self, flow: MattressFlow, data: MattressData, turn_context: TurnContext):
        """
        Slouží k získání informací o matraci, jakou zákazník požaduje
        :param flow: State machine pro ukládání stavu
        :param data: Přepravka na data
        :param turn_context: TurnContext
        :return: void
        """
        user_input = turn_context.activity.text.strip()
        intent, luis_result = await LuisHelper.execute_luis_query(
            self.luis_recognizer, turn_context
        )

        # Zeptáme se na rozměry matrace
        if flow.last_question_asked == MattressQuestion.NONE:
            await turn_context.send_activity(
                MessageFactory.text("Dobrý den, jaké rozměry by matrace měla mít?")
            )
            flow.last_question_asked = MattressQuestion.SIZE

        # Odešleme zprávu s tím co LUIS rozpoznal, jestli jsme vybrali správnou velikost
        elif flow.last_question_asked == MattressQuestion.SIZE:
            if self._validate_intent(intent, Intent.INTENT_SIZE):
                await turn_context.send_activity(
                    MessageFactory.text(f"Potřebujete matraci s rozměry '{Intent.get_text(intent)}'? (ano, ne)")
                )
                data.size = intent
                flow.last_question_asked = MattressQuestion.SIZE_VALIDATION
            else:
                await turn_context.send_activity(
                    MessageFactory.text(
                        "Vaší zprávě jsem nerozuměl. Zadejte prosím rozměry matrace znova (můžete zkusit: 110x200, 200 na 200, 150 * 200).")
                )

        # Zvalidujeme odpověď na předchozí výběr velikosti
        elif flow.last_question_asked == MattressQuestion.SIZE_VALIDATION:
            validate_result = self._validate(user_input)

            if validate_result is False:
                await turn_context.send_activity(
                    MessageFactory.text(
                        "Zadejte prosím rozměry matrace znovu.")
                )
                flow.last_question_asked = MattressQuestion.SIZE
            else:
                await turn_context.send_activity(
                    MessageFactory.text("Jakou tvrdost matrace požadujete?")
                )
                flow.last_question_asked = MattressQuestion.RIGIDITY

        # Zopakování tuhosti matrace
        elif flow.last_question_asked == MattressQuestion.RIGIDITY:
            if self._validate_intent(intent, Intent.INTENT_RIGIDITY):
                await turn_context.send_activity(
                    MessageFactory.text(f"Potřebujete matraci, která je '{Intent.get_text(intent)}'? (ano, ne)")
                )
                data.rigidity = intent
                flow.last_question_asked = MattressQuestion.RIGIDITY_VALIDATION
            else:
                await turn_context.send_activity(
                    MessageFactory.text("Vaší zprávě jsem nerozuměl. Zadejte prosím tvrdost matrace znova (můžete zkusit: tvrdá, tuhá, měkká, ...).")
                )

        # Validace tuhosti matrace
        elif flow.last_question_asked == MattressQuestion.RIGIDITY_VALIDATION:
            validate_result = self._validate(user_input)

            if validate_result is False:
                await turn_context.send_activity(
                    MessageFactory.text("Zadejte prosím jakou tvrdost matrace požadujete")
                )
                flow.last_question_asked = MattressQuestion.RIGIDITY
            else:
                await turn_context.send_activity(
                    MessageFactory.text("Z jakého materiálu by matrace měla být?")
                )
                flow.last_question_asked = MattressQuestion.MATERIAL

        # Zeptání se na materiál
        elif flow.last_question_asked == MattressQuestion.MATERIAL:
            if self._validate_intent(intent, Intent.INTENT_MATERIAL):
                await turn_context.send_activity(
                    MessageFactory.text(f"Hledáte matraci, která je '{Intent.get_text(intent)}'? (ano, ne)")
                )
                data.material = intent
                flow.last_question_asked = MattressQuestion.MATERIAL_VALIDATION
            else:
                await turn_context.send_activity(
                    MessageFactory.text("Vaši zprávě jsem nerozuměl. Zadejte prosím materiál matrace znova (můžete zkusit: pružinová, pěnová).")
                )

        # Validace materiálu
        elif flow.last_question_asked == MattressQuestion.MATERIAL_VALIDATION:
            validate_result = self._validate(user_input)

            if validate_result is False:
                await turn_context.send_activity(
                    MessageFactory.text("Zadejte prosím jaký materiál by matrace měla mít")
                )
                flow.last_question_asked = MattressQuestion.MATERIAL
            # Tady už se přesouváme do zadání údajů uživatele
            else:
                await turn_context.send_activity(
                    MessageFactory.text("Jaké je Vaše jméno a příjmení?")
                )
                flow.last_question_asked = MattressQuestion.DONE
                user_flow = await self.flow_accessor.get(turn_context, ConversationFlow)
                user_flow.last_question_asked = Question.NAME

    async def _fill_out_user_profile(self, flow: ConversationFlow, profile: UserProfile, turn_context: TurnContext):
        """
        Slouží ke získání údajů o zákazníkovi
        :param flow: flow pro řízení konverzace
        :param profile: přepravka pro data
        :param turn_context: TurnContext
        :return: void
        """
        user_input = turn_context.activity.text.strip()

        # Zadání jména zeptáme se o validaci
        if flow.last_question_asked == Question.NAME:
            await self._repeat(user_input, turn_context)
            profile.name = user_input
            flow.last_question_asked = Question.NAME_VALIDATION

        # Validujeme, že bylo správně zadané
        elif flow.last_question_asked == Question.NAME_VALIDATION:
            validate_result = self._validate(user_input)

            if validate_result is False:
                await turn_context.send_activity(
                    MessageFactory.text("Zadejte prosím jméno a příjmení znovu")
                )
                flow.last_question_asked = Question.NAME
            else:
                await turn_context.send_activity(
                    MessageFactory.text("Jaký je název Vaši ulice, včetně č.p.?")
                )
                flow.last_question_asked = Question.ADDRESS

        # Zopakujeme co uživatel napsal a požádáme o potvrzení
        elif flow.last_question_asked == Question.ADDRESS:
            await self._repeat(user_input, turn_context)
            profile.address = user_input
            flow.last_question_asked = Question.ADDRESS_VALIDATION

        # Vezmem odpověď od uživatele s potvrzením správnosti
        elif flow.last_question_asked == Question.ADDRESS_VALIDATION:
            validate_result = self._validate(user_input)

            if validate_result is False:
                await turn_context.send_activity(
                    MessageFactory.text("Zadejte prosím název Vaši ulice, včetně č.p. znovu")
                )
                flow.last_question_asked = Question.ADDRESS
            else:
                await turn_context.send_activity(
                    MessageFactory.text("Zadejte PSČ včetně města")
                )
                flow.last_question_asked = Question.ZIP

        # Požádáme o validaci ZIP
        elif flow.last_question_asked == Question.ZIP:
            await self._repeat(user_input, turn_context)
            profile.zip = user_input
            flow.last_question_asked = Question.ZIP_VALIDATION

        # Potvrzení ZIP
        elif flow.last_question_asked == Question.ZIP_VALIDATION:
            validate_result = self._validate(user_input)

            if validate_result is False:
                await turn_context.send_activity(
                    MessageFactory.text("Jaké je Vaše jméno a příjmení?")
                )
                flow.last_question_asked = Question.ZIP
            else:
                mattress_data = await self.mattress_accessor.get(turn_context, MattressData)
                user_profile = await self.profile_accessor.get(turn_context, UserProfile)

                with open("resources/SummaryCard.json", "rb") as in_file:
                    card_data = json.load(in_file)

                mattress_chooser = MattressChooser(mattress_data)
                mattress = mattress_chooser.pick_mattress()

                for item in card_data["body"]:
                    if "text" in item:
                        item["text"] = item["text"].replace("$SIZE", Intent.get_text(mattress.size))
                        item["text"] = item["text"].replace("$RIGIDITY", Intent.get_text(mattress.rigidity))
                        item["text"] = item["text"].replace("$MATERIAL", Intent.get_text(mattress.material))
                        item["text"] = item["text"].replace("$PRICE", mattress.price)
                        item["text"] = item["text"].replace("$MATTRESS_NAME", mattress.name)
                        item["text"] = item["text"].replace("$NAME", user_profile.name)
                        item["text"] = item["text"].replace("$ADDRESS", user_profile.address)
                        item["text"] = item["text"].replace("$ZIP", user_profile.zip)

                    if "url" in item:
                        item["url"] = item["url"].replace("$IMAGE_URL", self.config.IMAGE_SERVER + mattress.image_path)

                card = CardFactory.adaptive_card(card_data)

                message = Activity(
                    type=ActivityTypes.message,
                    attachments=[card]
                )

                await turn_context.send_activity(message)
                await turn_context.send_activity(
                    MessageFactory.text("Přejete si odeslat objednávku? (ano, ne)")
                )
                flow.last_question_asked = Question.CONFIRM_ADDRESS_VALIDATION

        # Potvrzení souhrnu osobních údajů
        elif flow.last_question_asked == Question.CONFIRM_ADDRESS_VALIDATION:
            validate_result = self._validate(user_input)

            if validate_result is True:
                await turn_context.send_activity(
                    MessageFactory.text("Děkujeme za Vaši objednávku")
                )

            mattress_flow = await self.mattress_flow_accessor.get(turn_context, MattressFlow)
            mattress_flow.last_question_asked = MattressQuestion.NONE

    async def _repeat(self, user_input: str, turn_context: TurnContext):
        await turn_context.send_activity(
            MessageFactory.text(f"Zadali jste \"{user_input}\", je to správně? (ano, ne)"),
        )

    def _validate_intent(self, intent: str, compare_to: str):
        return Intent.get_type(intent) == compare_to

    def _validate(self, user_input):
        if user_input == "ano" or user_input == "jo":
            return True
        elif user_input == "ne":
            return False
        else:
            return False
