from botbuilder.ai.luis import LuisApplication, LuisRecognizer
from botbuilder.core import Recognizer, TurnContext, RecognizerResult
from config import DefaultConfig


class MattressRecognizer(Recognizer):
    def __init__(self, configuration: DefaultConfig):
        self._recognizer = None

        luis_application = LuisApplication(
            configuration.LUIS_APP_ID,
            configuration.LUIS_API_KEY,
            configuration.LUIS_API_HOST_NAME
        )

        self._recognizer = LuisRecognizer(luis_application)

    async def recognize(self, turn_context: TurnContext) -> RecognizerResult:
        return await self._recognizer.recognize(turn_context)
