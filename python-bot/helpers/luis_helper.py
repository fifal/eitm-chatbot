from typing import Dict

from botbuilder.ai.luis import LuisRecognizer
from botbuilder.core import TurnContext, TopIntent, IntentScore


class Intent:
    MATTRESS_SIZE_SMALL = "MattressSizeSmall"
    MATTRESS_SIZE_MEDIUM = "MattressSizeMedium"
    MATTRESS_SIZE_LARGE = "MattressSizeLarge"
    MATTRESS_RIGIDITY_SOFT = "MattressRigiditySoft"
    MATTRESS_RIGIDITY_MEDIUM = "MattressRigidityMedium"
    MATTRESS_RIGIDITY_HARD = "MattressRigidityHard"
    MATTRESS_MATERIAL_FOAM = "MattressMaterialFoam"
    MATTRESS_MATERIAL_SPRING = "MattressMaterialSpring"

    NONE_INTENT = "None"

    INTENT_SIZE = "intent_size"
    INTENT_RIGIDITY = "intent_rigidity"
    INTENT_MATERIAL = "intent_material"

    @staticmethod
    def get_text(enum: str):
        """
        Vrací textový popisek nalezeného intentu
        :param enum: string s intentem
        :return: str
        """
        if enum == Intent.MATTRESS_SIZE_SMALL:
            return "110 x 200"
        elif enum == Intent.MATTRESS_SIZE_MEDIUM:
            return "150 x 200"
        elif enum == Intent.MATTRESS_SIZE_LARGE:
            return "200 x 200"

        elif enum == Intent.MATTRESS_RIGIDITY_SOFT:
            return "měkká"
        elif enum == Intent.MATTRESS_RIGIDITY_MEDIUM:
            return "středně tvrdá"
        elif enum == Intent.MATTRESS_RIGIDITY_HARD:
            return "tvrdá"

        elif enum == Intent.MATTRESS_MATERIAL_FOAM:
            return "pěnová"
        elif enum == Intent.MATTRESS_MATERIAL_SPRING:
            return "pružinová"

    @staticmethod
    def get_type(enum: str):
        """
        Vrací kategorii intentu podle intentu
        :param enum: intent
        :return: str
        """
        if enum == Intent.MATTRESS_SIZE_SMALL or \
                enum == Intent.MATTRESS_SIZE_MEDIUM or \
                enum == Intent.MATTRESS_SIZE_LARGE:
            return Intent.INTENT_SIZE

        elif enum == Intent.MATTRESS_RIGIDITY_SOFT or \
                enum == Intent.MATTRESS_RIGIDITY_MEDIUM or \
                enum == Intent.MATTRESS_RIGIDITY_HARD:
            return Intent.INTENT_RIGIDITY

        elif enum == Intent.MATTRESS_MATERIAL_SPRING or \
                enum == Intent.MATTRESS_MATERIAL_FOAM:
            return Intent.INTENT_MATERIAL


def top_intent(intents: Dict[Intent, dict]) -> TopIntent:
    max_intent = Intent.NONE_INTENT
    max_value = 0.0

    for intent, value in intents:
        intent_score = IntentScore(value)
        if intent_score.score > max_value:
            max_intent, max_value = intent, intent_score.score

    return TopIntent(max_intent, max_value)


class LuisHelper:
    @staticmethod
    async def execute_luis_query(
            luis_recognizer: LuisRecognizer, turn_context: TurnContext
    ) -> (Intent, object):
        """
        Returns an object with preformatted LUIS results for the bot's dialogs to consume.
        """
        result = None
        intent = None

        try:
            recognizer_result = await luis_recognizer.recognize(turn_context)

            intent = (
                sorted(
                    recognizer_result.intents,
                    key=recognizer_result.intents.get,
                    reverse=True,
                )[:1][0]
                if recognizer_result.intents
                else None
            )

        except Exception as exception:
            print(exception)

        return intent, result
