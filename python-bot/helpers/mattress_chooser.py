from data_models import MattressData
from helpers import Intent


class Mattress:
    def __init__(self, size=None, material=None, rigidity=None, name=None, price=None, image_path=None):
        self.size = size
        self.material = material
        self.rigidity = rigidity
        self.name = name
        self.price = price
        self.image_path = image_path


class MattressChooser:
    def __init__(self, mattress_data: MattressData):
        self.mattress_data = mattress_data
        self.mattress_list = [
            # Měkká, malá
            Mattress(Intent.MATTRESS_SIZE_SMALL, Intent.MATTRESS_MATERIAL_SPRING, Intent.MATTRESS_RIGIDITY_SOFT,
                     "Matrace BabySpring Soft", "1499", "resources/images/small-spring-soft.jpg"),
            Mattress(Intent.MATTRESS_SIZE_SMALL, Intent.MATTRESS_MATERIAL_FOAM, Intent.MATTRESS_RIGIDITY_SOFT,
                     "Matrace BabyFoam Soft", "2499", "resources/images/small-foam-soft.jpg"),

            # Střední, malá
            Mattress(Intent.MATTRESS_SIZE_SMALL, Intent.MATTRESS_MATERIAL_SPRING, Intent.MATTRESS_RIGIDITY_MEDIUM,
                     "Matrace BabySpring Medium", "1499", "resources/images/small-spring-medium.jpg"),
            Mattress(Intent.MATTRESS_SIZE_SMALL, Intent.MATTRESS_MATERIAL_FOAM, Intent.MATTRESS_RIGIDITY_MEDIUM,
                     "Matrace BabyFoam Medium", "2499", "resources/images/small-foam-medium.jpg"),

            # Tvrdá, malá
            Mattress(Intent.MATTRESS_SIZE_SMALL, Intent.MATTRESS_MATERIAL_SPRING, Intent.MATTRESS_RIGIDITY_HARD,
                     "Matrace BabySpring Hard", "1499", "resources/images/small-spring-hard.jpg"),
            Mattress(Intent.MATTRESS_SIZE_SMALL, Intent.MATTRESS_MATERIAL_FOAM, Intent.MATTRESS_RIGIDITY_HARD,
                     "Matrace BabyFoam Hard", "2499", "resources/images/small-foam-hard.jpg"),

            # Měkká, střední
            Mattress(Intent.MATTRESS_SIZE_MEDIUM, Intent.MATTRESS_MATERIAL_SPRING, Intent.MATTRESS_RIGIDITY_SOFT,
                     "Matrace KidSpring Soft", "2499", "resources/images/medium-spring-soft.jpg"),
            Mattress(Intent.MATTRESS_SIZE_MEDIUM, Intent.MATTRESS_MATERIAL_FOAM, Intent.MATTRESS_RIGIDITY_SOFT,
                     "Matrace KidFoam Soft", "3499", "resources/images/medium-foam-soft.jpg"),

            # Střední, střední
            Mattress(Intent.MATTRESS_SIZE_MEDIUM, Intent.MATTRESS_MATERIAL_SPRING, Intent.MATTRESS_RIGIDITY_MEDIUM,
                     "Matrace KidSpring Medium", "2499", "resources/images/medium-spring-medium.jpg"),
            Mattress(Intent.MATTRESS_SIZE_MEDIUM, Intent.MATTRESS_MATERIAL_FOAM, Intent.MATTRESS_RIGIDITY_MEDIUM,
                     "Matrace KidFoam Medium", "3499", "resources/images/medium-foam-medium.jpg"),

            # Tvrdá, střední
            Mattress(Intent.MATTRESS_SIZE_MEDIUM, Intent.MATTRESS_MATERIAL_SPRING, Intent.MATTRESS_RIGIDITY_HARD,
                     "Matrace KidSpring Hard", "2499", "resources/images/medium-spring-hard.jpg"),
            Mattress(Intent.MATTRESS_SIZE_MEDIUM, Intent.MATTRESS_MATERIAL_FOAM, Intent.MATTRESS_RIGIDITY_HARD,
                     "Matrace KidFoam Hard", "3499", "resources/images/medium-foam-hard.jpg"),

            # Měkká, velká
            Mattress(Intent.MATTRESS_SIZE_LARGE, Intent.MATTRESS_MATERIAL_SPRING, Intent.MATTRESS_RIGIDITY_SOFT,
                     "Matrace AdultSpring Soft", "3499", "resources/images/large-spring-soft.jpg"),
            Mattress(Intent.MATTRESS_SIZE_LARGE, Intent.MATTRESS_MATERIAL_FOAM, Intent.MATTRESS_RIGIDITY_SOFT,
                     "Matrace AdultFoam Soft", "4499", "resources/images/large-foam-soft.jpg"),

            # Střední, velká
            Mattress(Intent.MATTRESS_SIZE_LARGE, Intent.MATTRESS_MATERIAL_SPRING, Intent.MATTRESS_RIGIDITY_MEDIUM,
                     "Matrace AdultSpring Medium", "3499", "resources/images/large-spring-medium.jpg"),
            Mattress(Intent.MATTRESS_SIZE_LARGE, Intent.MATTRESS_MATERIAL_FOAM, Intent.MATTRESS_RIGIDITY_MEDIUM,
                     "Matrace AdultFoam Medium", "4499", "resources/images/large-foam-medium"),

            # Tvrdá, velká
            Mattress(Intent.MATTRESS_SIZE_LARGE, Intent.MATTRESS_MATERIAL_SPRING, Intent.MATTRESS_RIGIDITY_HARD,
                     "Matrace AdultSpring Hard", "3499", "resources/images/large-spring-hard.jpg"),
            Mattress(Intent.MATTRESS_SIZE_LARGE, Intent.MATTRESS_MATERIAL_FOAM, Intent.MATTRESS_RIGIDITY_HARD,
                     "Matrace AdultFoam Hard", "4499", "resources/images/large-foam-hard.jpg"),
        ]

    def pick_mattress(self) -> MattressData:
        for mattress in self.mattress_list:
            if mattress.size == self.mattress_data.size and \
                    mattress.rigidity == self.mattress_data.rigidity and \
                    mattress.material == self.mattress_data.material:
                self.mattress_data.name = mattress.name
                self.mattress_data.price = mattress.price
                self.mattress_data.image_path = mattress.image_path

                return self.mattress_data
