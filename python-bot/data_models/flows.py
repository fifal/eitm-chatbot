from enum import Enum


class Question(Enum):
    NONE = 0
    NAME = 1
    NAME_VALIDATION = 2
    ADDRESS = 3
    ADDRESS_VALIDATION = 4
    ZIP = 5
    ZIP_VALIDATION = 6
    CONFIRM_ADDRESS = 7
    CONFIRM_ADDRESS_VALIDATION = 8
    ORDER_SUMMARY = 9
    DONE = 9


class MattressQuestion(Enum):
    NONE = 0
    SIZE = 1
    SIZE_VALIDATION = 2
    RIGIDITY = 3
    RIGIDITY_VALIDATION = 4
    MATERIAL = 5
    MATERIAL_VALIDATION = 6
    DONE = 7


class MattressFlow:
    def __init__(self, last_question_asked: Question = MattressQuestion.NONE):
        self.last_question_asked = last_question_asked


class ConversationFlow:
    def __init__(self, last_question_asked: Question = Question.NONE):
        self.last_question_asked = last_question_asked
