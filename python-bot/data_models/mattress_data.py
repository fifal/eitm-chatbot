class MattressData:
    def __init__(self, size=None, rigidity=None, material=None, name=None, price=None, image_path=None):
        self.size = size
        self.rigidity = rigidity
        self.material = material
        self.name = name
        self.price = price
        self.image_path = image_path