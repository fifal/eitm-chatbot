class UserProfile:

    def __init__(self, name: str = None, address: str = None, zip: str = None):
        self.name = name
        self.address = address
        self.zip = zip
