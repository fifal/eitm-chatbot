<?php

declare(strict_types=1);

namespace App\Presenters;

use App\Components\IQuestionAnswerFactory;
use App\Components\IQuestionFormFactory;
use Nette;

/**
 * Class HomepagePresenter
 * @package App\Presenters
 *
 * @persistent(chatBotLuis)
 */
final class HomepagePresenter extends Nette\Application\UI\Presenter
{
    /**
     * @var IQuestionFormFactory
     */
    private $questionFormFactory;
    /**
     * @var IQuestionAnswerFactory
     */
    private $answerFactory;

    /**
     * @var string
     */
    private $chatbotSecretKey;
    /**
     * @var bool
     */
    private $isQnaEnabled;
    /**
     * @var string
     */
    private $chatbotEndpoint;

    public function __construct(string $chatbotEndpoint,
                                string $chatbotSecretKey,
                                bool $isQnaEnabled,
                                IQuestionFormFactory $questionFormFactory,
                                IQuestionAnswerFactory $answerFactory)
    {
        parent::__construct();

        $this->questionFormFactory = $questionFormFactory;
        $this->answerFactory = $answerFactory;

        $this->chatbotSecretKey = $chatbotSecretKey;
        $this->isQnaEnabled = $isQnaEnabled;
        $this->chatbotEndpoint = $chatbotEndpoint;
    }

    public function renderDefault()
    {
        $this->template->chatbotSecretKey = $this->chatbotSecretKey;
        $this->template->chatbotEndpoint = $this->chatbotEndpoint;
    }

    public function renderFaq()
    {
        $this->template->isQnaEnabled = $this->isQnaEnabled;
    }

    /**
     * @return \App\Components\QuestionAnswer
     */
    public function createComponentQuestionAnswer()
    {
        return $this->answerFactory->create();
    }

    /**
     * @return \App\Components\QuestionForm
     */
    public function createComponentQuestionForm()
    {
        return $this->questionFormFactory->create();
    }
}
