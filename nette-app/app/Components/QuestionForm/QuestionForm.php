<?php


namespace App\Components;


use App\Services\QNAChatBot;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class QuestionForm extends Control
{
    /**
     * @var QNAChatBot
     */
    private $chatbotService;

    public function __construct(QNAChatBot $chatbotService)
    {
        $this->chatbotService = $chatbotService;
    }

    public function render()
    {
        $this->template->render(__DIR__ . '/QuestionForm.latte');
    }

    public function createComponentForm()
    {
        $form = new Form();
        $form->setHtmlAttribute('class', 'ajax');

        $form->addText('question', 'Question');
        $form->addSubmit('submit', 'Submit');

        $form->onSuccess[] = [$this, 'formSubmitted'];
        return $form;
    }

    public function formSubmitted(Form $form)
    {
        $values = $form->getValues();
        $response = $this->chatbotService->getAnswer($values['question']);
        $this->presenter['questionAnswer']->updateResponse($response);
    }
}

interface IQuestionFormFactory
{
    /**
     * @return QuestionForm
     */
    public function create();
}