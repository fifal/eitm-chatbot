<?php


namespace App\Components;


use Nette\Application\UI\Control;

class QuestionAnswer extends Control
{
    protected $answer = [];

    public function render(){
        $this->template->answer = $this->answer;

        $this->template->render(__DIR__ . '/QuestionAnswer.latte');
    }

    public function updateResponse(array $response)
    {
        $this->answer = $response;

        $this->presenter->redrawControl('answerSnippet');
    }
}

interface IQuestionAnswerFactory
{
    /**
     * @return QuestionAnswer
     */
    public function create();
}