<?php

declare(strict_types=1);

namespace App\Services;


class QNAChatBot
{
    protected $endpoint;
    protected $knowledgeBase;
    protected $authorizationKey;

    public function __construct($endpoint, $knowledgeBase, $authorizationKey)
    {
        $this->endpoint = $endpoint;
        $this->knowledgeBase = $knowledgeBase;
        $this->authorizationKey = $authorizationKey;
    }

    /**
     * Získá odpověď z chatbota a vrátí je jako array
     *
     * @param string $question
     * @return array
     */
    public function getAnswer($question): array
    {
        $data = [
            "question" => $question
        ];

        $headers = [
            "Authorization: EndpointKey $this->authorizationKey",
            "Content-type: application/json"
        ];

        $endpoint = $this->endpoint . $this->knowledgeBase;

        $result = CurlService::post($endpoint, json_encode($data), $headers);
        $resultArray = json_decode($result, TRUE);

        if ($resultArray == NULL)
        {
            return [];
        }

        return $resultArray;
    }
}