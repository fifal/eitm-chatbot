<?php

declare(strict_types=1);

namespace App\Services;

/**
 * Slouží pro ulehčení práce s CURL dotazama
 * @package App\Services
 */
class CurlService
{
    /**
     * Složí k odeslání POST požadavku
     *
     * @param string $endpoint : URL kam se požadavek posílá
     * @param string $data : JSON Data požadavku
     * @param null|array $header : Data headeru
     * @return string
     */
    public static function post(string $endpoint, string $data, array $header = NULL): ?string
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $endpoint);
        curl_setopt($curl, CURLOPT_POST, 1);

        if ($data != NULL)
        {
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }
        if ($header != NULL)
        {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        }

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);

        curl_close($curl);

        // Pokud nastala chyba vracíme NULL a zalogujeme
        if (!$result)
        {
            \Tracy\Debugger::log("Nastala chyba při CURL požadavku: " . curl_error($curl), "CurlService.log");
            return NULL;
        }

        return $result;
    }

    /**
     * Slouží k odeslání GET požadavku
     *
     * @param string $endpoint
     * @param array|NULL $header
     * @return string|null
     */
    public static function get(string $endpoint, array $header = NULL): ?string
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $endpoint);

        if ($header != NULL)
        {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        }

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);

        curl_close($curl);

        // Pokud nastala chyba vracíme NULL a zalogujeme
        if (!$result)
        {
            \Tracy\Debugger::log("Nastala chyba při CURL požadavku: " . curl_error($curl), "CurlService.log");
            return NULL;
        }

        return $result;
    }
}