let form = $("#chatbotForm");
let errorMessage = $("#errorMessage");
let chatBotWindow = $("#chatbotWindow");
let chatBot = $("#chatbot");
let postUrl = $("[name='postUrl']").val();

/**
 * Handlování formuláře pro chatování
 */
form.submit(function (e) {
    e.preventDefault();

    // Prázdná zpráva, zobrazíme hlášku
    let messageText = $("#messageText");
    if (!validateInput(messageText)) {
        return;
    }

    showChatBot();

    addUserMessage(messageText);
    sendQuestion(messageText);
});

/**
 * Odešle zprávu s otázkou na backend
 *
 * @param messageText
 */
function sendQuestion(messageText) {
    let question = messageText.val();

    /**
     * Zakázání odesílání
     */
    function disableSend() {
        messageText.prop('disabled', true);
        messageText.val('');
    }

    /**
     * Povolení odesílání
     */
    function enableSend() {
        messageText.prop('disabled', false);
        messageText.focus();
    }

    $.ajax({
        type: "POST",
        url: postUrl,
        data: {"message": question},
        beforeSend: function () {
            disableSend();
        },
        success: function (data) {
            let json = $.parseJSON(data);
            addBotMessage(json.answer);
            scrollToBottom();
            enableSend();
        },
        error: function () {
            enableSend();
        }
    })
}

/**
 * Scrollování do spodku okna po každé odeslané zprávě
 */
function scrollToBottom() {
    chatBotWindow.get(0).scrollTo(0, chatBotWindow.get(0).scrollHeight);
}

/**
 * Přidá uživatelskou zprávu do chatbota
 *
 * @param message Object: input type text
 */
function addUserMessage(message) {
    let htmlTemplate = "<div class=\"row\">\n" +
        "    <div class=\"col-4 offset-8\">\n" +
        "        <div class=\"chat-bubble bg-primary text-white\">\n" +
        "            <div>" + message.val() + "</div>\n" +
        "        </div>\n" +
        "    </div>\n" +
        "</div>";

    let html = $.parseHTML(htmlTemplate);
    chatBotWindow.append(html);
    scrollToBottom();
}

/**
 * Přidá zprávu od bota
 *
 * @param message string
 */
function addBotMessage(message) {
    let htmlTemplate = "<div class=\"row\">\n" +
        "    <div class=\"col-4\">\n" +
        "        <div class=\"chat-bubble bg-light-gray\">\n" +
        "            <div>" + message + "</div>\n" +
        "        </div>\n" +
        "   </div>\n" +
        "</div>";

    let html = $.parseHTML(htmlTemplate);
    chatBotWindow.append(html);
}

/**
 * Zobrazí chatovací okno
 */
function showChatBot() {
    chatBot.prop("style", "display: block");
}

/**
 * Funkce zobrazí chybovou hlášku, pokud je odesílaná zpráva prázdná
 *
 * @param input
 */
function validateInput(input) {
    if (input.val() === "") {
        input.prop("class", "form-control mt-4 is-invalid");
        errorMessage.prop("style", "display: block");

        return false;
    } else {
        input.prop("class", "form-control mt-4");
        errorMessage.prop("style", "display: none");

        return true;
    }
}